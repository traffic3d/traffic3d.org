# Vehicle behaviour

Most vehicle behaviour is calculated using `VehicleSensors`.
[**Click here**](./traffic3d_assets.md#vehicle-sensors) to find out more about sensors.

## Turning speed control

Vehicles control their speed while turning, using a custom Traffic3D algorithm.
The algorithm uses the following steps to calculate the target speed of the vehicle:

1. The vehicle scans ahead along the road for a specific amount of meters and calculates a position on the road.

1. Using the position, the vehicle calculates the angle (degrees) between the current vehicle direction and the direction of the position.

1. Steps 1 - 2 repeat, using multiple distances of 10, 30, 60, 150 meters.

1. The mean of the angles is calculated.

1. The mean angle is used in the following formula:

    * Where:
        * `m` = Sensitivity Modifier (Default value of 50).
        * `a` = Average Angle Difference (Degrees).
        * `t` = Corner Threshold (Default value of 90 Degrees)
    * `target_speed = (m^(-a/t)) * maxSpeed`
    * Where:
        * `m` is a sensitivity modifier (no units, default is 50).
        * `a` is the average angle difference (in degrees).
        * `t` is the corner threshold (in degrees, default is 90deg)

1. The speed output is then set as the target speed.

## Collision sensors

By placing multiple rays from the vehicle, the vehicle can detect whether there is an obstacle with a collider in front of its self.
These can be made more sensitive by increasing the `Number of Sensor Rays` attribute on the vehicle object, although this may decrease performance.
If the vehicle is turning, the rays will move with the vehicle, which helps to detect obstacles around a corner.

## Stop lines and merging

When a vehicle first spawns into the scene, all *intersections* (where paths of other vehicles overlap with the current vehicle's path) are calculated and stored in a list.

Once a vehicle pulls up to a [**Stop Line**](traffic3d_assets.md#stop-lines), the vehicle evaluates whether the path is clear by:

1. Finding the intersections in a certain radius around the vehicle, this is `mergeRadiusCheck` in the `VehicleEngine` class, and defaults to 100m.
1. Then for each of the intersections, the vehicle:
    1. Calculates the *current* distance to the intersection.
    1. Calculates the *other* vehicle's distance to the intersections.
    1. Calculates the *relative distance* from each other.
    1. Applies the _ramps algorithm_ from Nagel and Rasmussen (1994) to the values above, to generate a target speed.

The speed of the vehicle is then set to the minimum of the target speeds calculated by the algorithm above.

Vehicles behave slightly differently, depending on whether the stop line is a `MERGE` or `JUNCTION`, but the algorithm above applies to both cases.
Find out more on the [assets page](traffic3d_assets.md#stop-lines).

### Notes

* All intersections are currently calculated on a *2D plane* (meaning that any roads that are above another road would class as an intersection) but will later support the 3D Environment.
* Traffic3D uses flat coordinates - i.e. it assumes that the earth is flat. This should not affect most scenes.

## Deadlock release system

When multiple vehicles are merging at multiple stop lines, deadlocks can occur when multiple vehicles are waiting for each other, before they proceed past the stop line.
To stop this from happening, Traffic3D contains a deadlock release system, which randomly releases vehicles which are deadlocked at junctions.
Each vehicle has its own deadlock release system, which replicates the actions of a driver in the same situation.
The system is built as a [vehicle sensor](traffic3d_assets.md#vehicle-sensors) and works in the following way:

1. After running the [merge sensor](vehicle_behaviour.md#stop-lines-and-merging), the deadlock release system detects a deadlock by looping through all vehicles the current vehicle is waiting on. If the vehicle finds itself in the list, or the last vehicle is waiting on the front collision sensors, a deadlock has been found.
1. Once a deadlock is detected, Traffic3D will continue to re-test for deadlocks for a specified amount of time e.g. 3 seconds. If the deadlock has resolved itself within that time, the deadlock release system will not intervene. If the time period has passed, and the deadlock is still unresolved, the system proceeds to the next step.
1. Each vehicle has its own deadlock priority, which is a float between `0` and `1`, with 1 being the highest priority. Using the `Merge Radius Check` distance, the current vehicle will order every other vehicle within the radius by deadlock priority value.
1. Each vehicle checks its own vehicle priority list. If the vehicle finds itself at the top of the list, it will release itself from the stop line if its front collision sensors are clear.
1. The released vehicle will drive cautiously through the junction using the `Deadlock release proceed speed` value for its target speed.
1. The deadlock is checked again, and if it has still not resolved, the system will keep attempting releases every `5` seconds until no deadlock is detected.

## Further reading

* Nagel, K., & Rasmussen, S. (1994). Traffic at the edge of chaos (No. LA-UR-94-2681). Los Alamos National Laboratory.
