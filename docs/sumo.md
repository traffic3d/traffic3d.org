# SUMO

SUMO allows for traffic systems to be modelled. [Click Here](https://www.eclipse.org/sumo/) to find out more about SUMO.

## How to connect to SUMO

Connecting Traffic3D with SUMO allows for SUMO simulations to be viewed and controlled in Traffic3D using the settings from the [**Sumo Manager**](./traffic3d_assets.md#sumo-manager).
Currently, this features allows Traffic3D and SUMO to control the following:

* Traffic Flow
* Traffic Lights

The buildings and lampposts are automatically generated and will adapt to the road network.
Once running, Traffic3D will render the vehicles on the road according to their positions in SUMO.
To connect to SUMO, follow the instructions below:

1. Within Unity, load the Sumo Scene which is found at `/Assets/Scenes/Sumo.unity`.
1. Edit the *Sumo Manager* options within the `SumoManager` game object.
1. Navigate to the `Assets/Sumo/` folder and remove the original `map.net` and `map.rou` files.
1. Find or Create a SUMO network (`.net file`) and a SUMO demand (`.rou`) file to use.
1. In [**NetEdit**](https://sumo.dlr.de/docs/netedit.html), with the SUMO network and demand file loaded, press `Edit > Open in SUMO-GUI`.
1. Save the configuration in **SumoGui** with `File > Save Configuration` and close SumoGui.
1. Add the following xml within the `configuration` xml tag in the `.sumocfg` file created when saving the configuration in the previous step.

    ```xml
    <traci_server>
       <remote-port value="4001"/>
    </traci_server>

    <step-length value="0.016" />
    ```

1. Edit the port if needed according to the port displayed in the `Sumo Manager`. The step length is your simulation time. The current value of `0.016` is close to real time, increase this if needed.
1. Get the `.net` and `.rou` files created in the previous steps, copy them into the `Assets/Sumo/` folder and rename them to `map.net` and `map.rou`.
1. Open SumoGui, press `File > Open Simulation` and use the configuration file that was previously edited. The SumoGui console should output `***Starting server on port 4001 ***`.
1. Finally press the play button on Unity and it should load the SUMO Network into Traffic3D with the chosen options from `Sumo Manager`.
