# Build scenes with OpenStreetMap

OpenStreetMap allows **road networks around the globe** to be downloaded as a map file and these files can be **imported into Traffic3D** to quickly generate scenes at a large scale.

## Download an OpenStreetMap file

Follow the instructions below to **download a map file** from OpenStreetMap:

1. [Click Here](http://www.openstreetmap.org/) to visit OpenStreetMap.

1. Search the name of the **location** you wish to model in the textbox.

    ![OpenStreetMap Searchbar](./assets/images/Osm_searchbar.png)

    Alternatively, click and drag the map around until you find a location you like.

1. Zoom into the page using the icons provided by OpenStreetMaps or your middle mouse scroll wheel, to **capture the part of the road network** you wish to model.

    ![OpenStreetMap Zoom Buttons](./assets/images/Osm_zoom.png)

    Everything displayed on screen screen will be downloaded into the map file.

    OpenStreetMaps has a downloadable limit of **50,000 nodes**.

1. To bring up the **export UI**, select the upper `Export` button.

    ![OpenStreetMap Upper Export Button](./assets/images/Osm_export_button_1.png)

1. To download the map file, select the UI `Export` button.

    ![OpenStreetMap UI Export Button](./assets/images/Osm_export_button_2.png)

    If download failed due to the file exceeding 50,000 nodes, zoom into the page or drag the map around until less roads and buildings are visible.

## Import an OpenStreetMap file

Follow the instructions below to **import a file** from OpenStreetMap into Traffic3D:

1. OpenStreetMap files are `.osm` files and must be converted into a text file by **renaming** the file extension to `.txt`.

1. Open up Traffic3D in Unity and add your `.txt` file.

    ![OpenStreetMap file in unity](./assets/images/Osm_unity_mapfile.png)

1. Bring up the **GUI** to import map data: `Traffic3D` > `Import OpenStreetMap File`.

    ![Import OpenStreetMap Tabs](./assets/images/Osm_unity_gui_tab.png)

1. Select your **desired map file** and the **textures/materials** to apply to the roads, buildings and floor.

    Select the circular buttons on the right to choose your textures.

    ![Import OpenStreetMap GUI](./assets/images/Osm_unity_gui_materials.png)

    > ***Note***: You will be unable to select `Import OpenStreetMap File` if you are **not in a new Unity Scene**.

1. After a few seconds, your scene will render and the only remaining requirement is to customise the **Vehicle Factory**.

    Information about the Vehicle Factory can be found at [Traffic3D Assets](./traffic3d_assets.md#vehicle-factory).

    After modifying the Vehicle Factory the simulation can be run.

    > ***Note***: It is advised that the top-speed and turning speed of vehicles are reduced to prevent vehicles accelerating too quickly towards corners and driving off the roads.

## Modify a road's mesh

In the [Unity inspector](./unity.md#edit-properties) you will find a list of game objects.

A road will consist of a parent object, with **two child objects**.

![Road with two child objects](./assets/images/Osm_unity_road_parent_object_children.png)

* **First Child Object** - Contains the road mesh, a `Road Mesh Updater` component and has a child object positioned above the **centre node**, which displays the **road name**.

* **Second Child Object** - Contains a `RoadWay` component and has child objects representing nodes; information about the `RoadWay` component can be found at [Traffic3D Assets](./traffic3d_assets.md#the-road-network).

After modifying a path, the road's **mesh** can be **quickly updated** by selecting the `Update Road Mesh` button in the Road Mesh Updater component.

![Road Mesh Updater attached to game object](./assets/images/Osm_unity_road_mesh_updater_game_object.png)

### Road Mesh Updater

![Road Mesh Updater Component](./assets/images/Osm_unity_road_mesh_updater_component.png)

This is used to quickly update a road mesh by only requiring nodes in a `Path` to be modified, and will automatically modify the mesh when the `Update Road Mesh` button is selected.

The script has the following fields:

* **Road** - The game object the script is attached too, which should also contain the `Mesh Renderer` and `Mesh Filter` components.
* **Path Object** - A game object with the Path component attached, and shares the same parent object as `Road`.
* **Num Lanes** - The number of lanes in the road.
* **Lane Width** -  The width of each lane in meters.
* **Update Road Mesh** - A button which will change the shape of the mesh in the `Road` field to match the shape of the path in the `Path Object` field.

### Stop Line Renderer

![Road Mesh Updater Component](./assets/images/StopLineRenderer.png)

[Stop Lines](./traffic3d_assets.md#stop-lines) in OSM are automatically added to certain road nodes throughout the simulation.
When moving the stop lines, you may have to replace the render of the stop line.
This can be done by clicking on the relevant node and then clicking the `Re-render Stop Line` button.
Adjust the `Render Width` to extend the stop line, which is normally the width of the road.

To change the model of the stop line:

1. Open the following prefab in Unity `Assets/Resources/Models/StopLineRender.prefab`
1. Edit the object or replace this file with your own at the same file path.

## Change road layouts

If the map has not been imported correctly or if a new road is being planned, you may need to change the road layout.

Definitions:

* *Road way object* - An object within the `RoadNetwork` object that has `_Path` on the end of its name and has the `RoadWay` script attached.

Here are some examples below:

### Changing a roadway by moving one node

1. Click on the road way object you want to change.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/SimpleRoadMove1.png)
1. Find the `RoadWay` script in the inspector and find the node you want to move in the `nodes` list.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/SimpleRoadMove2.png)
1. Click on the node and it should be selected in the Hierarchy window.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/SimpleRoadMove3.png)
1. Once selected, use the move tool to move the node in the new location.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/SimpleRoadMove4.png)
1. Repeat the previous steps to move other nodes into different places if needed.
1. After this, the road mesh will need updating. On the same road way object, find the `Road Mesh Updater` script and click the `Update Road Mesh` button. This should then realign the road correctly.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/SimpleRoadMove5.png)
1. If you have moved a node which has other roads attached, then do the same to the attached roads until all roads look correct.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/SimpleRoadMove6.png)
1. If a road has a stop line connected, then you may also need to [re-render the stop line](import_open_street_map.md#stop-line-renderer).

### Disconnecting a road

1. Click on the road way object you want to disconnect.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/SimpleDisconnectRoad1.png)
1. In some cases, if it is only a short road way connecting both roads, deleting the road way object completely will disconnect the roads.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/SimpleDisconnectRoad2.png)
1. Otherwise, select the node that is connecting both roads.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/AdvancedDisconnectRoad1.png)
1. Remove the node from the list. Make sure to rearrange the nodes so that the empty list element is at the end of the list. For example, if the start node was removed, then move the remaining nodes up the list.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/AdvancedDisconnectRoad2.png)
1. Update the list size to remove the empty last element in the list.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/AdvancedDisconnectRoad3.png)
1. Update the relevant road meshes and stop lines like the [changing a roadway by moving one node](import_open_street_map.md#changing-a-roadway-by-moving-one-node) section.
   ![Traffic Light Configuration](./assets/images/osm/changing-roads/AdvancedDisconnectRoad4.png)

## Adding Street Furniture Models

Please note that *street lights* do not rely on OSM data and are automatically placed along roads.

1. Find the name of the Open Street Map [Amenity](https://wiki.openstreetmap.org/wiki/Key:amenity) or [Emergency](https://wiki.openstreetmap.org/wiki/Key:emergency) item.

1. Navigate to `\Assets\Resources\Models\OpenStreetMap\` then depending on which category the item is, navigate into either the `Amenities\` or `Emergency\` folder.

1. Add a prefab model to the folder and rename it to the name found in step one e.g. Adding a post box model will mean re-naming the prefab model to `post_box.prefab`.

1. Import the OSM file as normal, then your models should appear where specified on OSM. If they do not appear, try checking the name of the prefab, as this needs to be exactly the same name as the item.

## Limitations

The current version of the import system has some restrictions on the types of networks which can be modelled:

* Only grid systems, such as those found in American cities, can currently be modelled as the system **only supports cross junctions**.

* Variations in **road heights**, such as tunnels and bridges, are not supported and as all roads will be generated at the same Y-level.
