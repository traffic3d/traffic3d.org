# Traffic3D setup

## Control Panel

The control panel is the main UI for managing Traffic3D.
To open the control panel, click the `Traffic3D` tab in the menu bar and then click `Simulation Controller`.

![Traffic3D Tab](./assets/images/Traffic3DTab.png)

![Control Panel](./assets/images/ControlPanel.png)

### Current Scene

This button changes the scene that is being used for the simulation.
Use this to load the demo scenes or your own scenes.

### Simulation Controls

These three buttons control the main simulation.

* **Play** - Starts the simulation.
* **Pause** - Temporarily stops the simulation.
* **Stop** - Completely stops the simulation.

### Options

Each button is a shortcut to the relevant options and settings.
For example, `Open VehicleFactory Options` opens the settings to the vehicle factory such as maximum number of vehicles.
Hover over the buttons for more information about each of the settings.

### Miscellaneous

* **Random Seed** - The current seed to produce random numbers in the simulation using [C# Random class](https://docs.microsoft.com/en-us/dotnet/api/system.random?view=net-5.0).
* **Capture Rate** - Changes the simulation speed by updating the `Frame Rate` in the [PythonManager script](./traffic3d_assets.md#python-manager)

## Creating a scene

Before creating a scene, view the [Traffic3D Assets Page](./traffic3d_assets.md) to understand how all the Traffic3D objects work.

First, copy the **Demo scene** and save as your own scene. This will give a base to work on.

Delete any unneeded **terrain** assets out of the scene and then start with creating the terrain that is needed for your simulation.

![Creating the Scene](./assets/images/CreatingTheScene.png)

Edit the [**Environment Settings**](./traffic3d_assets.md#environment-settings) to suit needs of the simulation.

### Traffic Lights

Either use the prefab or create your [**Traffic Lights**](./traffic3d_assets.md#traffic-lights) which at minimum should have a main object, and 3 circle objects to light up as red, amber and green.
On the main object, add the `TrafficLight.cs` script.

Fill in everything except the **Stop Nodes and Current Light Colour**.
Repeat this for all the traffic lights in the scene while remembering to fill in the Traffic Light Id with **unique numbers that increment** starting from one.

Add some [**Pedestrian Crossings**](./pedestrians/traffic3d_pedestrians.md#pedestrian-crossings) if needed, this will allow pedestrians to cross the road at relevant.

### Junctions

Create a [**Junction**](./traffic3d_assets.md#junctions) and register the traffic lights to each junction as needed.
Then add junction states for the traffic lights and pedestrian crossings.

### Roads

[**The Road Network**](./traffic3d_assets.md#the-road-network) needs to be setup for the vehicles to calculate [**Vehicle Paths**](./traffic3d_assets.md#vehicle-paths).
Start by creating an empty game object and attach the `RoadNode` script.
Create a few `RoadNode`s in a line.

Create another empty game object and attach the `RoadWay` script.
Within the `RoadWay` object, there is an ordered list to add as many `RoadNode`s as you wish.
You should add `RoadNode`s in the order in which you want vehicles to travel along the way.
The `RoadNode` at the top of the list will be the start of the way.
A single node can be attached to multiple ways which connects the ways together.
Click on the way, and a line will appear between the nodes.

![Creating a way](assets/images/CreatingAWay.png)

Nodes at traffic lights (the stop nodes) need to be added in the `Stop Node` list within the traffic light script of the relevant traffic light.
For example, a `RoadNode`, that was placed for `trafficlight1`, would be placed into the list of `Stop Nodes` on `trafficlight1`'s script.

[**Stop Lines**](./traffic3d_assets.md#stop-lines) can be added to manage traffic further by allowing vehicles to evaluate whether it is appropriate to continue at a junction.
These are normally added when a lane is merging into another lane or on a junction without traffic lights.
Stop Lines are similar to [give way / yield signs](https://en.wikipedia.org/wiki/Yield_sign) which tells a driver to slow down and wait for other vehicles to pass through the junction first before continuing.
Traffic3D does not draw visuals for Stop Lines (road markings or signs) when a script is attached, so any road markings or signs would need to be placed manually.

The [**VehicleFactory**](./traffic3d_assets.md#vehicle-factory) game object needs a list of vehicles and their probabilities of spawning in the scene.

### Pedestrians

The pedestrians need to be added by going to the [**PedestrianFactory**](./pedestrians/traffic3d_pedestrians.md#pedestrian-factory) game object and placing pedestrian models inside the list.
Probability will also need to be specified.
Within the `PedestrianFactory`, `PedestrianPoints` need to be added as seen below:

![Pedestrian Points](./assets/images/PedestrianPoints.png)

[**Pedestrian Points**](./pedestrians/traffic3d_pedestrians.md#pedestrian-point) are placed where a pedestrian needs to spawn and / or go to.

## Creating a Vehicle

To create a new vehicle, find a model online or create your own model and put wheel colliders on each of the wheels.

Tag the vehicle with `car` using the Tag field in the top of the inspector.

![Creating a Vehicle](./assets/images/CreatingAVehicle.png)

Add the `Vehicle` and `VehicleSettings` scripts onto the parent object of the vehicle.

[**Click here**](./traffic3d_assets.md#vehicles) to view the main Vehicle scripts.
Edit the values as needed.

Once finished with the vehicle, save it as a prefab and place the prefab into the vehicle factory.
Check the car spawns into the scene, setting the probability to `1` and changing the rest of the vehicles to `0` can help test this.

## Creating a Pedestrian

Find a pedestrian model online or create a model with a humanoid shape.
Added the `Pedestrian.cs` script onto the model along with the following other components:

* Animator - Using `PedestrianAnimationController` as the controller.
* Nav Mesh Agent
* Capsule Collider - Set is trigger to true.
* Rigidbody - Set is kinematic as true

![Creating a Vehicle](./assets/images/CreatingAPedestrian.png)

Feel free to copy a prefab from `Assets/Resources/Models/Pedestrians` and use the same values.
[**Click here**](./pedestrians/traffic3d_pedestrians.md#pedestrians) to find out more about editing the values of the Pedestrian Script.
