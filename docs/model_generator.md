# Working with the Python3 model generator

This code comes with a model generator, which generates stochastic events for the simulation.
The model generator is written in Python3 and can be found in the `backend` directory.
The [**Python Manager**](traffic3d_assets.md#python-manager) is used by the model generator to link to Traffic3D.

To use it, first install the requirements in a [Python virtual environment](https://docs.python-guide.org/dev/virtualenvs/).

For Linux, use the following commands:

```sh
sudo apt-get install python-virtualenv
cd backend
virtualenv --python=/usr/bin/python3.7 venv
. venv/bin/activate
pip install -r requirements.txt
pip install torch==1.1.0
```

If **Windows** is being used then use the following commands and then use the following link to download **PyTorch**: [https://pytorch.org/](https://pytorch.org/)

```sh
py -m venv .
cd scripts
activate.bat
cd ..
pip install -r requirements.txt
(Insert command from the PyTorch website)
```

Then run the code, which will listen for a socket from the Unity application:

```sh
$ python traffic3d_processor.py
waiting for tcpConnection
```

## Using the PyCharm IDE

[PyCharm](https://www.jetbrains.com/pycharm/) is an IDE for Python can be used to setup the backend easily.
It automatically installs the virtual environment.

1. Open up the `backend` folder as a project.
1. In settings `Ctrl+Alt+S`, `project: backend` > `project interpreter`, press the cog and press `Add...`
1. Press `Ok` and `Apply`
1. By using the `Terminal` in the bottom left of the screen, install requirements.txt and torch.
1. The Terminal can then be used to run `python traffic3d_processor.py`.

PyCharm can also be used to debug the script by using breakpoints if needed.

More information about PyCharm can be found here: [https://www.jetbrains.com/help/pycharm/meet-pycharm.html](https://www.jetbrains.com/help/pycharm/meet-pycharm.html)

## Writing a custom model generator

This code comes with a model generator, which generates stochastic events for the simulation.
To use your own model generator, you need to extend the `ModelGenerator` class, which can be found in the `/backend/model_generator.py` file:

```python
import model_generator

class Traffic3DProcessor(model_generator.ModelGenerator):
```

Next, add the constructor and call the constructor in the superclass:

```python
def __init__(self):
    super().__init__()
```

The constructor can have as many or as few parameters as needed.
If a different port is needed then use the following:

```python
def __init__(self, port):
    super().__init__(port)
```

`enable()` is an abstract method which needs to be implemented into the new class.
It is called once the socket is setup, a connection has been made between Unity and the model generator, the screenshots folder path has been sent to Unity and finally a max amount of junction states have been received from Unity.

Within the `ModelGenerator` class there are multiple methods that interact with the traffic simulation.

* `receive_images()` - used when the script needs to grab all screenshots from every junction from the simulation. This method blocks the main thread.
* `send_action(action)` - used to send the actions for every junction to the simulation which is a json object of the actions with the corresponding junction id.
* `receive_rewards()` - used to receive the rewards from the simulation. This method blocks the main thread.

At the end of the script, be sure to create an instance of the class:

```python
Traffic3DProcessor(IMAGES_PATH)
```

To see an example of this look at the Traffic3DProcessor in the `/backend/traffic3d_processor.py` file.
