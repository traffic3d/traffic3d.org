# Traffic3D: A Rich 3D-Traffic Environment to Train Intelligent Agents

![Traffic3D Front Page Image](./assets/images/FrontPageImage.jpg)

Traffic3D is a new traffic simulation paradigm, built to push forward research in human-like learning (for example, based on photo-realistic visual input).
It provides a fast, cheap and scalable proxy for real-world traffic environments.
This implies effective simulation of diverse and dynamic 3D-road traffic scenarios, closely mimicking real-world traffic characteristics such as faithful simulation of individual vehicle behaviour, their precise physics of movement and photo-realism.
Traffic3D can facilitate research across multiple domains, including reinforcement learning, object detection and segmentation, unsupervised representation learning and visual question answering.

<div class='embed-container'>
   <iframe width="560" height="315" src="https://www.youtube.com/embed/OoNb_zzdhg8" title="Introduction to Traffic3D" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## New publication at AAMAS 2022

This video describes our latest paper, [Fully-Autonomous, Vision-based Traffic Signal Control: from Simulation to Reality](https://publications.aston.ac.uk/id/eprint/43543/) has recently been presented at [AAMAS 2022](https://aamas2022-conference.auckland.ac.nz/).

<div class='embed-container'>
   <iframe width="560" height="315" src="https://www.youtube.com/embed/jqacwLi6w9k" title="Fully-Autonomous, Vision-based Traffic Signal Control: from Simulation to Reality
" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

Please see [below](index.md#bibliography) for a full bibliography.

## Evacu-agent

Evacu-agent was an undergraduate final year project to extend Traffic3D to provide pedestrians with a framework to model realistic behaviour.

It is now integrated into the Traffic3D repository and can be explored in more detail in [Evacu-agent introduction](pedestrians/evacu-agent/introduction.md).

## Technologies

Traffic3D is based on the [Unity 3d games engine](https://unity3d.com/unity).
The AI is written in [Python3](https://www.python.org/) with [PyTorch](https://pytorch.org/).

## Supported platforms

Traffic3D is tested on 64-bit Windows, Linux and OSX.

## Download Traffic3D

Download the latest pre-built version of Traffic3D here:

* [Windows 64-bit](https://gitlab.com/traffic3d/traffic3d/-/jobs/artifacts/master/download?job=build-StandaloneWindows64)
* [Linux 64-bit](https://gitlab.com/traffic3d/traffic3d/-/jobs/artifacts/master/download?job=build-StandaloneLinux64)
* [OSX](https://gitlab.com/traffic3d/traffic3d/-/jobs/artifacts/master/download?job=build-StandaloneOSX)

To download the source code, please visit [this page](https://gitlab.com/traffic3d/traffic3d/-/tree/master) and click on the **Download** button (to the left of the **Clone** button).

You can see a list of Traffic3d releases [on this page](https://gitlab.com/traffic3d/traffic3d/-/releases).

## Limitations

The current release of Traffic3D does not include **autonomous vehicles**, although users are encouraged to create their own autonomous vehicle scripts.

All vehicles currently move along pre-set paths.

## Citing Traffic3D

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3968432.svg)](https://doi.org/10.5281/zenodo.3968432)

The [bibliography](index.md#bibliography) below contains a list of papers that describe the research on which Traffic3D is based.
However, to cite the software itself, please see the details in the [CITATION file](https://gitlab.com/traffic3d/traffic3d/blob/master/CITATION) in the main repository.

The [Software Sustainability Institute](https://software.ac.uk/) has a guide on [citing software](https://software.ac.uk/how-cite-software), which may be helpful to some.

## Bibliography

### Journal publications

D. Garg, M. Chli and G. Vogiatzis, 2019, June. [Traffic3D: A Rich 3D-Traffic Environment to Train Intelligent Agents.](https://publications.aston.ac.uk/id/eprint/39553/1/ICCS_Garg2019.pdf) In International Conference on Computational Science (pp. 749-755). Springer.

### Articles in blind peer-reviewed conferences

D. Garg, M. Chli and G. Vogiatzis, 2022, [Fully-Autonomous, Vision-based Traffic Signal Control: from Simulation to Reality](https://publications.aston.ac.uk/id/eprint/43543/), Proceedings of the International Conference of Autonomous Agents and Multiagent Systems - AAMAS 2022.

D. Garg, M. Chli and G. Vogiatzis, 2020, [Multi-agent Deep Reinforcement Learning for Traffic Optimisation through Multiple Road Intersections using Live Camera Feed](https://ieeexplore.ieee.org/abstract/document/9294375), IEEE Intelligent Transportation Systems Conference - ITSC 2020.

D. Garg, M. Chli and G. Vogiatzis, 2019, [A Deep Reinforcement Learning Agent for Traffic Intersection Control Optimisation](https://ieeexplore.ieee.org/abstract/document/8917361), IEEE Intelligent Transportation Systems Conference - ITSC 2019.

D. Garg, M. Chli and G. Vogiatzis, 2019, May. [Traffic3D: A New Traffic Simulation Paradigm](http://www.ifaamas.org/Proceedings/aamas2019/pdfs/p2354.pdf) In Proceedings of the 18th International Conference on Autonomous Agents and MultiAgent Systems (pp. 2354-2356). International Foundation for Autonomous Agents and Multiagent Systems.

D. Garg, M. Chli and G. Vogiatzis, 2018, September. [Deep Reinforcement Learning for Autonomous Traffic Light Control.](http://www.george-vogiatzis.org/publications/ICITE2018.pdf) In 2018 3rd IEEE International Conference on Intelligent Transportation Engineering (ICITE) (pp. 214-218). IEEE.

D. Garg, M. Chli and G. Vogiatzis, 2018, [Deep Reinforcement Learning for Autonomous Traffic Light Control](https://ieeexplore.ieee.org/abstract/document/8492537) In Proceedings of the 3rd IEEE International Conference on Intelligent Transportation Engineering.
